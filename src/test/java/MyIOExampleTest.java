import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

public class MyIOExampleTest {
    MyIOExample example = new MyIOExample();

    /**
     * Успешное выполение метода workWithFile
     */
    @Test
    public void workWithFileTest() {
        boolean isExists = example.workWithFile("i.txt");
        Assertions.assertTrue(isExists);
    }

    /**
     * Успешное выполение метода copyBufferedFile
     */
    @Test
    public void copyBufferedFileTest() {
        boolean isCopyBuf = example.copyBufferedFile("i.txt", "i_copyBuf.txt");
        Assertions.assertTrue(isCopyBuf);
    }

    /**
     * Успешное выполение метода copyFileWithReaderAndWriter
     */
    @Test
    public void copyFileWithReaderAndWriterTest() {
        boolean isCopyFiles = example.copyFileWithReaderAndWriter("i.txt", "i_copyFiles.txt");
        Assertions.assertTrue(isCopyFiles);
    }

    /**
     * Неуспешное выполение метода copyBufferedFile
     */
    @Test
    public void copyBufferedFileNegativeTest() {
        boolean isCopyBuf = example.copyBufferedFile("i2.txt", "i_copyBuf.txt");
        Assertions.assertFalse(isCopyBuf);
    }

    /**
     * Неуспешное выполение метода copyFileWithReaderAndWriter
     */
    @Test
    public void copyFileWithReaderAndWriterNegativeTest() {
        boolean isCopyFiles = example.copyFileWithReaderAndWriter("i2.txt", "i_copyFiles.txt");
        Assertions.assertFalse(isCopyFiles);
    }
}
