import org.junit.Assert;
import org.junit.Test;
import sbp.Person;
import sbp.db.PersonDao;

import java.util.ArrayList;
import java.util.List;

public class PersonTest {
    PersonDao dao = new PersonDao();
    Person person = new Person("Lena", 20);
    Person person1 = new Person("Dima", 25);
    Person person2 = new Person("Olya", 25);
    Person person3 = new Person("Oleg", 34);
    /**
     * Успешное выполнение метода createPerson
     */
    @Test
    public void createTest() {
        dao.createPerson(person);
        List<Person> person1 = new ArrayList<>();
        person1.add(person);
        List<Person> listFromBd = dao.readAllPerson();
        Person personFromBd =null;
        for (Person p:listFromBd){
            if (p.getName().equals("Lena")){
                personFromBd =p;
            }
        }
        Assert.assertEquals(person, personFromBd);
    }

    /**
     * Успешное выполнение метода updatePerson
     */
    @Test
    public void updatePersonTest() {
        dao.createPerson(person2);
        dao.updatePerson("Olya", 44);
        int[] age = dao.readAllPerson().stream()
                .filter(person -> person.getName().equals("Olya"))
                .mapToInt(person2 -> person2.getAge()).toArray();
        Assert.assertEquals(age[0], 44);
    }

    /**
     * Успешное выполнение метода createPerson
     */
    @Test
    public void readAllPersonTest() {
        List<Person> listFromBd = dao.readAllPerson();
        int sizeBefore = listFromBd.size();
        dao.createPerson(person1);
        listFromBd = dao.readAllPerson();
        Assert.assertEquals(sizeBefore+1, listFromBd.size());
    }

    /**
     * Успешное выполнение метода deletePerson
     */
    @Test
    public void deleteTest() {
        dao.createPerson(person3);
        List<Person> listFromBd = dao.readAllPerson();
        int sizeBefore = listFromBd.size();
        dao.deletePerson("Oleg");
        listFromBd = dao.readAllPerson();
        Assert.assertEquals(sizeBefore-1, listFromBd.size());
    }

    /**
     * Неуспешное выполнение метода updatePerson
     */
    @Test
    public void updatePersonNegativeTest() {
        int result = dao.updatePerson("Lena1", 44);
        Assert.assertEquals(0, result);
    }

    /**
     * Неуспешное выполнение метода deletePerson
     */
    @Test
    public void deleteNegativeTest() {
        int result = dao.deletePerson("Dima1");
        Assert.assertEquals(0, result);
    }

}
