import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import sbp.ArrayIterator;
import sbp.exceptions.CustomArrayException;

public class ArrayIteratorTest {
    private ArrayIterator<Integer> customArray;
    private Integer[][] arrayInt = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void beforeMethod() {
        customArray = new ArrayIterator<>(arrayInt);
    }

    /**
     * Успешное выполнение метода size
     */
    @Test
    public void sizeTest() {
        Assert.assertEquals(3, customArray.size());
        Integer[] arrayInt = {11, 12, 13};
        customArray.add(arrayInt);
        Assert.assertEquals(4, customArray.size());
    }

    /**
     * Успешное выполнение метода isEmpty
     */
    @Test
    public void isEmptyTest() {
        Assert.assertFalse(customArray.isEmpty());
    }

    /**
     * Успешное выполнение метода add
     */
    @Test
    public void addTest() {
        Integer[] arrayInt = {11, 12, 13};
        Assert.assertTrue(customArray.add(arrayInt));
    }

    /**
     * Успешное выполнение метода get
     */
    @Test
    public void getTest() {
        Integer[] arrayInt = {1, 2, 3};
        Assert.assertArrayEquals(arrayInt, customArray.get(0));
    }

    /**
     * Успешное выполнение метода set
     */
    @Test
    public void setTest() {
        Integer[] arrayInt = {11, 12, 13};
        Assert.assertEquals(3, customArray.size());
        customArray.set(0, arrayInt);
        Assert.assertEquals(4, customArray.size());
    }

    /**
     * Успешное выполнение метода remove(int index)
     */
    @Test
    public void removeIndexTest() {
        Assert.assertEquals(3, customArray.size());
        customArray.remove(0);
        Assert.assertEquals(2, customArray.size());
    }

    /**
     * Успешное выполнение метода remove(T item)
     */
    @Test
    public void removeTest() {
        Integer[] arrayInt = {1, 2, 3};
        Assert.assertEquals(3, customArray.size());
        customArray.remove(arrayInt);
        Assert.assertEquals(2, customArray.size());
    }

    /**
     * Успешное выполнение метода contains()
     */
    @Test
    public void containsTest() {
        Integer[] arrayInt = {1, 2, 3};
        Integer[] arrayInt2 = {1, 4, 3};
        Assert.assertTrue(customArray.contains(arrayInt));
        Assert.assertFalse(customArray.contains(arrayInt2));
    }

    /**
     * Успешное выполнение метода indexOf()
     */
    @Test
    public void indexOfTest() {
        Integer[] arrayInt = {1, 2, 3};
        Assert.assertEquals(0, customArray.indexOf(arrayInt));
    }

    /**
     * Успешное выполнение метода ensureCapacity()
     */
    @Test
    public void ensureCapacityTest() {
        Assert.assertEquals(3, customArray.size());
        customArray.ensureCapacity(customArray.size() + 1);
        Integer[] arrayInt = {1, 8, 3};
        customArray.add(arrayInt);
        Assert.assertEquals(4, customArray.size());
    }

    /**
     * Успешное выполнение метода getCapacity()
     */
    @Test
    public void getCapacityTest() {
        Assert.assertEquals(3, customArray.getCapacity());
    }

    /**
     * Успешное выполнение метода reverse()
     */
    @Test
    public void reverseTest() {
        Integer[] arrayInt = {1, 2, 3};
        Assert.assertArrayEquals(arrayInt, customArray.get(0));
        customArray.reverse();
        Integer[] arrayRev = {7, 8, 9};
        Assert.assertArrayEquals(arrayRev, customArray.get(0));
    }

    /**
     * Успешное выполнение метода toArray()
     */
    @Test
    public void toArrayTest() {
        Object[] newArray = customArray.toArray(1);
        Integer[] arrayInt = {4, 5, 6};
        Assert.assertArrayEquals(newArray,arrayInt);
    }

    /**
     * Неуспешное выполнение метода indexOf()
     */
    @Test
    public void indexOfNegativeTest() {
        Integer[] arrayInt = {4, 9, 6};
        Assert.assertEquals(-1, customArray.indexOf(arrayInt));
    }

    /**
     * Неуспешное выполнение метода remove(int index)
     */
    @Test
    public void removeIndexNegativeTest() {
        expectedException.expect(CustomArrayException.class);
        expectedException.expectMessage("Index is not found!");
        customArray.remove(5);
    }
}