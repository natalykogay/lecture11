package sbp;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import sbp.exceptions.CustomArrayException;

import java.util.Arrays;
import java.util.List;

@ToString
@EqualsAndHashCode
public class ArrayIterator<T> {
    private T[][] date;
    private int size = 0;
    private final int DEFAULT_CAPACITY = 5;


    @SuppressWarnings("unchecked")
    public ArrayIterator(T[][] array) {
        date = array;
        size = array.length;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean add(T[] item) {
        ensureCapacity(date.length + 1);
        date[size++] = item;
        return true;
    }


    public T[] get(int index) {
        if (index > date.length) {
            throw new CustomArrayException("The length is less than index!");
        }
        return date[index];
    }

    public T[] set(int index, T[] item) {
        T[][] newAllDate = (T[][]) new Object[size + 1][];
        System.arraycopy(date, 0, newAllDate, 0, index);
        newAllDate[index] = item;
        System.arraycopy(date, index, newAllDate, index + 1, size - index);
        size++;
        date = newAllDate;
        return item;
    }

    public void remove(int index) {
        if (index > size) {
            throw new CustomArrayException("Index is not found!");
        }
        int elementToCopy = size - index - 1;
        if (elementToCopy > 0) {
            System.arraycopy(date, index + 1, date, index, elementToCopy);
            date[--size] = null;
        }
    }

    public boolean remove(T[] item) {
        boolean isTrue = false;
        List<T> list = Arrays.asList(item);
        for (int index = 0; index < size; index++) {
            List<T> list2 = Arrays.asList(date[index]);
            if (list.equals(list2)) {
                int elementToCopy = size - index - 1;
                if (elementToCopy > 0) {
                    System.arraycopy(date, index + 1, date, index, elementToCopy);
                    date[--size] = null;
                    isTrue = true;
                }
            }
        }
        return isTrue;
    }

    public boolean contains(T[] item) {
        for (int i = 0; i < date.length; i++) {
            List<T> list = Arrays.asList(item);
            List<T> list2 = Arrays.asList(date[i]);
            if (list.equals(list2)) {
                return true;
            }
        }
        return false;
    }

    public int indexOf(T[] item) {
        int index = -1;
        for (int i = 0; i < date.length; i++) {
            List<T> list = Arrays.asList(item);
            List<T> list2 = Arrays.asList(date[i]);
            if (list.equals(list2)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount > date.length) {
            T[][] newAllDate = (T[][]) new Object[newElementsCount][];
            System.arraycopy(date, 0, newAllDate, 0, size);
            date = newAllDate;
        }
    }

    public int getCapacity() {
        return date.length;
    }

    public void reverse() {
        T[][] newAllDate = (T[][]) new Object[date.length][];
        int indexNew = 0;
        for (int indexDate = date.length - 1; indexDate >= 0; indexDate--) {
            newAllDate[indexNew++] = date[indexDate];
        }
        date = newAllDate;
    }

    public Object[] toArray(int index) {
        Object[] newArray = date[index];
        return newArray;
    }

}
