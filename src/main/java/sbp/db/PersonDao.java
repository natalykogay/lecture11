package sbp.db;

import sbp.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDao {
    private static final String URL = "jdbc:sqlite:C:\\Обучение Sber\\sqlite\\sqlite-tools-win32-x86-3370200\\person.db";

    /**
     * Метод создает запись в таблице Person
     *
     * @param person
     */
    public void createPerson(Person person) {
        final String sql = "insert into Person(name,age) values(?,?)";
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, person.getName());
            preparedStatement.setInt(2, person.getAge());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод читает все записи из таблицы Person
     */
    public List<Person> readAllPerson() {
        final String sql = "Select * from Person";
        List<Person> list = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                Person person = new Person(
                        resultSet.getString("name"),
                        resultSet.getInt("age"));
                list.add(person);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    /**
     * Метод удаляет запись по name из таблицы Person
     *
     * @param name
     */
    public int deletePerson(String name) {
        int result;
        final String sql = "delete from Person where name = '" + name + "'";
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            result = 0;
        }
        return result;
    }

    public int updatePerson(String name, int age) {
        int result;
        final String sql = "update Person set age = " + age + " where name = '" + name + "'";
        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement()) {
            result = statement.executeUpdate(sql);
        } catch (SQLException e) {
            result = 0;
            e.printStackTrace();
        }
        return result;
    }
}
