package sbp.exceptions;

public class CustomArrayException extends RuntimeException {
    /**
     * Конструктор {@code CustomArrayException без сообщения об ошибке.
     */
    public CustomArrayException() {
    }

    /**
     * Конструктор {@code CustomArrayException с детальным сообщением об ошибке.
     *
     * @param message сообщение при получении ошибки.
     */
    public CustomArrayException(String message) {
        super(message);
    }
}
