package sbp.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MyIOExample {
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) {
        Path path = Paths.get(fileName);
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName))) {
            writer.write("FileName + " + fileName);
            writer.write('\n');
            writer.write("write by newBufferedWriter");
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean isExists = Files.exists(path);
        System.out.println("isExists = " + isExists);
        if (isExists) {
            System.out.println("getParent = " + path.getParent());
            System.out.println("toAbsolutePath = " + path.toAbsolutePath());
            boolean isFile = Files.isRegularFile(path);
            System.out.println("isFile = " + isFile);
            if (isFile) {
                try {
                    System.out.println("size = " + Files.size(path));
                    System.out.println("LastModifiedTime = " + Files.getLastModifiedTime(path));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return isExists;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) {
        Path path = Paths.get(sourceFileName);
        Path pathCopy = null;
        if (Files.exists(path)) {
            try {
                pathCopy = Files.copy(path, Paths.get(destinationFileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Files.exists(pathCopy);
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) {
        Path path = Paths.get(sourceFileName);
        boolean isCopy = false;
        if (Files.exists(path)) {
            try (BufferedReader reader = Files.newBufferedReader(path);
                 BufferedWriter writer = Files.newBufferedWriter(Paths.get(destinationFileName))) {
                String line = "";
                while ((line = reader.readLine()) != null) {
                    writer.write(line);
                    writer.write('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            isCopy = Files.exists(Paths.get(destinationFileName));
        }
        return isCopy;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) {
        Path path = Paths.get(sourceFileName);
        Path pathCopy = null;
        boolean isCopy = false;
        if (Files.exists(path)) {
            try {
                List<String> list = Files.readAllLines(path);
                pathCopy = Files.write(Paths.get(destinationFileName), list);
            } catch (IOException e) {
                e.printStackTrace();
            }
            isCopy = Files.exists(Paths.get(destinationFileName));
        }
        return isCopy;
    }
}
